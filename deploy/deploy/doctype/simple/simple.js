// Copyright (c) 2016, Ivan Ray Altomera and contributors
// For license information, please see license.txt
//
// frappe.ui.form.on('Simple', {
//
//     onload: function (frm) {
//         frm.set_value('age', getAge(frm.doc.date));
//         frm.toggle_display('fav_alcohol', frm.doc.age >= 18);
//         // console.log(frm);
//     },
//     'date': function (frm) {
//         frm.set_value('age', getAge(frm.doc.date));
//     },
//     'age': function (frm) {
//         frm.toggle_display('fav_alcohol', frm.doc.age >= 18);
//     }
//
// });

frappe.ui.form.on('Simple', 'onload', function (frm) {
    frm.set_value('age', getAge(frm.doc.date));
    frm.toggle_display('fav_alcohol', frm.doc.age >= 18);
});

frappe.ui.form.on('Simple', 'date', function (frm) {
    frm.set_value('age', getAge(frm.doc.date));
});

frappe.ui.form.on('Simple', 'age', function (frm) {
    frm.toggle_display('fav_alcohol', frm.doc.age >= 18);
});

frappe.ui.form.on('Simple', 'customer', function (frm) {
    // console.log('hello');
    // console.log(frm.doc.customer);
    frappe.call({
        "method": "frappe.client.get",
        args: {
            doctype: "Customer",
            name: frm.doc.customer
        },
        callback: function (data) {
            //console.log(data);
            cur_frm.set_value('name_2', data.message.customer_name);
            refresh_field("name");

        }
    });

    //
    frappe.call({
       "method": "deploy.deploy.doctype.simple.simple.get_movies",
        args: {},
        callback: function (data) {
            console.log(data);
            frm.set_df_property('movies_allowed', 'options', data.message);
            frm.refresh_field('movies_allowed');
        }

    });

});

function getAge(date) {

    var birth = new Date(date);
    var cur = new Date();
    var diff = cur - birth;
    var age = Math.floor(diff / 31557600000);

    return age;

}