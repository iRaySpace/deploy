# -*- coding: utf-8 -*-
# Copyright (c) 2015, Ivan Ray Altomera and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

@frappe.whitelist()
def get_movies():
    print "***********GET MOVIES*************"
    ret = frappe.db.sql("""SELECT movie_title FROM tabMovies""")
    return ret

class Simple(Document):
    pass